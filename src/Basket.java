import java.util.Arrays;

public class Basket {
    Product[] purchasedProducts;

    public Basket(Product[] purchasedProducts) {
        this.purchasedProducts = purchasedProducts;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "purchasedProducts=" + Arrays.toString(purchasedProducts) +
                '}';
    }
}
