public class Exercise06 {
    public static void main(String[] args) {

        Product iphone = new Product("Iphone", 50000, 4.7);
        Product galaxy = new Product("Galaxy", 50000, 4.7);
        Product macbook = new Product("Macbook", 100000, 4.8);
        Product magicbook = new Product("Magicbook", 55000, 4.8);
        Product airpods = new Product("Airpods", 15000, 4.7);
        Product airdots = new Product("Airdots", 1500, 4.7);

        Category smartphones = new Category("Smartphones", new Product[]{iphone, galaxy});
        Category laptops = new Category("Laptops", new Product[]{macbook, magicbook});
        Category accessories = new Category("Accessories", new Product[]{airpods, airdots});
        System.out.println(smartphones);
        System.out.println(laptops);
        System.out.println(accessories);

        System.out.println();

        Product[] purchasedProducts = new Product[]{macbook, airdots};
        User buyer = new User("User1", "User1", new Basket(purchasedProducts));
        System.out.println(buyer);
    }
}
